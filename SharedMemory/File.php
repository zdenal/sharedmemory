<?php

class SharedMemory_File extends SharedMemory_Base
{

	/**
	 * Contains internal options
	 *
	 * @var string
	 */
	protected $_options;

	/**
	 * Constructor. Init all variables.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		$this->_options = $this->_default($options, array
					(
					'tmp' => '/tmp',
				));

		if ( ! is_writeable($this->_options['tmp']) OR ! is_dir($this->_options['tmp']))
		{
			throw new Exception('Tmp path is not writeable or is not a dir.');
		}
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 *
	 * @return mixed TRUE on success or PEAR_error on fail
	 */
	public function get($name, $default = NULL)
	{
		$name = $this->_options['tmp'].'/smf_'.md5($name);

		if ( ! file_exists($name))
		{
			return $default;
		}

		$fp = fopen($name, 'rb');
		if (is_resource($fp))
		{
			flock($fp, LOCK_SH);

			$str = fread($fp, filesize($name));
			fclose($fp);
			return $str == '' ? $default : unserialize($str);
		}

		throw new Exception('Cannot open file.');
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 *
	 * @return mixed TRUE on success or PEAR_error on fail
	 */
	public function set($name, $value)
	{
		$fp = fopen($this->_options['tmp'].'/smf_'.md5($name), 'ab');
		if (is_resource($fp))
		{
			flock($fp, LOCK_EX);
			ftruncate($fp, 0);
			fseek($fp, 0);

			fwrite($fp, serialize($value));
			fclose($fp);
			clearstatcache();
			return TRUE;
		}

		throw new Exception('Cannot write to file.');
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return mixed TRUE on success or PEAR_error on fail
	 */
	public function rm($name)
	{
		$name = $this->_options['tmp'].'/smf_'.md5($name);

		if (file_exists($name))
		{
			unlink($name);
		}
	}

}

?>