<?php

class SharedMemory_Sqlite extends SharedMemory_Base
{

	/**
	 * SQLite object handler
	 *
	 * @var object
	 */
	protected $_h;
	/**
	 * hash of SQLite table options
	 *
	 * @var array
	 */
	protected $_options;

	/**
	 * Constructor. Init all variables.
	 * SQLite table must be created:
	 * CREATE sharedmemory(var text PRIMARY KEY, value TEXT)
	 * It's very important!
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		$this->_options = $this->_default($options, array
					(
					'db' => ':memory:',
					'table' => 'sharedmemory',
					'var' => 'var',
					'value' => 'value',
					'persistent' => FALSE,
				));

		$func = $this->_options['persistent'] ? 'sqlite_popen' : 'sqlite_open';

		$this->_h = $func($this->_options['db']);

		if ( ! is_resource($this->_h))
		{
			throw new Exception('Sqlite not connected');
		}
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name name of variable
	 *
	 * @return mixed value of the variable
	 */
	public function get($name, $default = NULL)
	{
		$name = sqlite_escape_string($name);
		$sql = 'SELECT '.$this->_options['value'].
				'FROM '.$this->_options['table'].
				'WHERE '.$this->_options['var'].'=\''.$name.'\''.
				'LIMIT 1';

		$result = sqlite_query($this->_h, $sql);
		if (sqlite_num_rows($result))
		{
			return unserialize(sqlite_fetch_single($result));
		}

		return $default;
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value)
	{
		$name = sqlite_escape_string($name);
		$value = sqlite_escape_string(serialize($value));

		$sql = 'REPLACE INTO '.$this->_options['table'].
				' ('.$this->_options['var'].', '.$this->_options['value'].
				'VALUES (\''.$name.'\', \''.$value.'\')';

		sqlite_query($this->_h, $sql);
		return TRUE;
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name)
	{
		$name = sqlite_escape_string($name);

		$sql = 'DELETE FROM '.$this->_options['table'].
				' WHERE '.$this->_options['var'].'=\''.$name.'\'';

		sqlite_query($this->_h, $sql);
		return TRUE;
	}

}

?>