<?php

class SharedMemory_Systemv extends SharedMemory_Base
{

	/**
	 * handler for shmop_* functions
	 *
	 * @var string
	 */
	protected $_h;

	/**
	 * Constructor. Init all variables.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		extract($this->_default($options, array
					(
					'size' => FALSE,
					'project' => 's',
				)));

		if ($size === FALSE)
		{
			$this->_h = shm_attach($this->_ftok($project));
		}
		else
		{
			$this->_h = shm_attach($this->_ftok($project), $size);
		}
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param int $name name of variable
	 *
	 * @return mixed value of the variable
	 */
	public function get($name, $default = NULL)
	{
		$name = $this->_s2i($name);
		
		return shm_has_var($this->_h, $name) 
				? unserialize(shm_get_var($this->_h, $name))
				: $default;
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 *
	 * @return bool TRUE on success, FALSE on fail
	 */
	public function set($name, $value)
	{
		return shm_put_var($this->_h, $this->_s2i($name), serialize($value));
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success, FALSE on fail
	 */
	public function rm($name)
	{
		$name = $this->_s2i($name);
		
		return shm_has_var($this->_h, $name) 
				? shm_remove_var($this->_h, $name)
				: TRUE;
	}

	/**
	 * ftok emulation for Windows
	 *
	 * @param string $project project ID
	 */
	protected function _ftok($project)
	{
		if (function_exists('ftok'))
		{
			return ftok(__FILE__, $project);
		}

		$s = stat(__FILE__);
		return sprintf("%u", (($s['ino'] & 0xffff) | (($s['dev'] & 0xff) << 16) |
				(($project & 0xff) << 24)));
	}

	/**
	 * convert string to int
	 *
	 * @param string $name string to conversion
	 */
	protected function _s2i($name)
	{
		$int = unpack('N', str_pad($name, 4, "\0", STR_PAD_LEFT));
		return $int[1];
	}

}
?>