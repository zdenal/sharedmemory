<?php

class SharedMemory_Eaccelerator extends SharedMemory_Base
{

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name name of variable
	 *
	 * @return mixed value of the variable
	 */
	public function get($name, $default = NULL)
	{
		return $this->_unpack(eaccelerator_get($name), $default);
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 * @param int $ttl (optional) time to life of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value, $ttl = 0)
	{
		eaccelerator_lock($name);
		return eaccelerator_put($name, $this->_pack($value), $ttl);
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name)
	{
		return eaccelerator_rm($name);
	}

}

?>