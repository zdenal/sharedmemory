<?php

class SharedMemory_Apc extends SharedMemory_Base
{

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name name of variable
	 *
	 * @return mixed value of the variable
	 */
	public function get($name, $default = NULL)
	{
		return apc_exists($name) ? apc_fetch($name) : $default;
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 * @param int $ttl (optional) time to life of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value, $ttl = 0)
	{
		return apc_store($name, $value, $ttl);
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name)
	{
		return apc_delete($name);
	}

}

?>