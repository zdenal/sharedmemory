<?php

class SharedMemory_Shmop extends SharedMemory_Base
{

	/**
	 * handler for shmop_* functions
	 *
	 * @var string
	 */
	protected $_h;
	/**
	 * Contains internal options
	 *
	 * @var string
	 */
	protected $_options;

	/**
	 * Constructor. Init all variables.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		$this->_options = $this->_default($options, array
					(
					'size' => 1048576,
					'tmp' => '/tmp',
					'project' => 's'
				));

		$this->_h = $this->_ftok($this->_options['project']);
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param mixed $name name of variable or FALSE if all variables needs
	 *
	 * @return mixed value of the variable
	 * @throws Exception
	 */
	public function get($name, $default = NULL)
	{
		$ret = $this->_get_all();
		
		return isset($ret[$name]) ? $ret[$name] : $default;
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param mixed $name name of variable or FALSE if all variables needs
	 *
	 * @return mixed value of the variable
	 * @throws Exception
	 */
	protected function _get_all()
	{
		$id = shmop_open($this->_h, 'c', 0600, $this->_options['size']);

		if ($id !== FALSE)
		{
			$ret = unserialize(shmop_read($id, 0, shmop_size($id)));
			shmop_close($id);

			return $ret;
		}

		throw new Exception('Cannot open shmop.');
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value)
	{
		$lh = $this->_lock();
		$val = $this->_get_all();
		if ( ! is_array($val))
		{
			$val = array();
		}

		$val[$name] = $value;
		$val = serialize($val);
		return $this->_write($val, $lh);
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name)
	{
		$lh = $this->_lock();
		$val = $this->_get_all();
		if ( ! is_array($val))
		{
			$val = array();
		}
		unset($val[$name]);
		$val = serialize($val);

		return $this->_write($val, $lh);
	}

	/**
	 * ftok emulation for Windows
	 *
	 * @param string $project project ID
	 */
	protected function _ftok($project)
	{
		if (function_exists('ftok'))
		{
			return ftok(__FILE__, $project);
		}

		$s = stat(__FILE__);
		return sprintf("%u", (($s['ino'] & 0xffff) | (($s['dev'] & 0xff) << 16) |
				(($project & 0xff) << 24)));
	}

	/**
	 * write to the shared memory
	 *
	 * @param string $val values of all variables
	 * @param resource $lh lock handler
	 *
	 * @return mixed TRUE on success
	 * @throws Exception
	 */
	protected function _write(&$val, &$lh)
	{
		$id = shmop_open($this->_h, 'c', 0600, $this->_options['size']);
		if ($id)
		{
			$ret = shmop_write($id, $val, 0) == strlen($val);
			shmop_close($id);
			$this->_unlock($lh);
			return $ret;
		}

		$this->_unlock($lh);
		throw new Exception('Cannot write to shmop.');
	}

	/**
	 * access locking function
	 *
	 * @return resource lock handler
	 */
	protected function &_lock()
	{
		if (function_exists('sem_get'))
		{
			$fp = PHP_VERSION < 4.3 ? sem_get($this->_h, 1, 0600) : sem_get($this->_h, 1, 0600, 1);
			sem_acquire($fp);
		}
		else
		{
			$fp = fopen($this->_options['tmp'].'/sm_'.md5($this->_h), 'w');
			flock($fp, LOCK_EX);
		}

		return $fp;
	}

	/**
	 * access unlocking function
	 *
	 * @param resource $fp lock handler
	 */
	protected function _unlock(&$fp)
	{
		if (function_exists('sem_get'))
		{
			sem_release($fp);
		}
		else
		{
			fclose($fp);
		}
	}

}
?>