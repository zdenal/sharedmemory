<?php

class SharedMemory_Memcache extends SharedMemory_Base
{

	/**
	 * Memcache object instance
	 *
	 * @var object
	 */
	protected $_mc;

	/**
	 * Constructor. Init all variables.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		extract($this->_default($options, array
					(
					'host' => '127.0.0.1',
					'port' => 11211,
					'timeout' => FALSE,
					'persistent' => FALSE,
				)));

		$func = $persistent ? 'pconnect' : 'connect';

		$this->_mc = &new Memcache;

		$connected = $timeout === FALSE ?
				$this->_mc->$func($host, $port) :
				$this->_mc->$func($host, $port, $timeout);

		if ( ! $connected)
		{
			throw new Exception('Memcache not connected.');
		}
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name name of variable
	 *
	 * @return mixed value of the variable
	 */
	public function get($name, $default = NULL)
	{
		return $this->_unpack($this->_mc->get($name), $default);
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 * @param int $ttl (optional) time to life of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value, $ttl = 0)
	{
		return $this->_mc->set($name, $this->_pack($value), 0, $ttl);
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name, $ttl = FALSE)
	{
		return $ttl === FALSE ?
				$this->_mc->delete($name) :
				$this->_mc->delete($name, $ttl);
	}

}

?>