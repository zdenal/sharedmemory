<?php

class SharedMemory_Apachenote extends SharedMemory_Base
{

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name name of variable
	 *
	 * @return mixed value of the variable
	 */
	public function get($name, $default = NULL)
	{
		$value = apache_note($name);
		if ($value === FALSE)
		{
			return $default;
		}
		return unserialize($value);
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value)
	{
		apache_note($name, serialize($value));
		return TRUE;
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name)
	{
		apache_note($name, FALSE);
		return TRUE;
	}

}

?>