<?php

class SharedMemory_Sharedance extends SharedMemory_Base
{

	/**
	 * connection handler
	 *
	 * @var string
	 */
	protected $_h;
	/**
	 * Contains internal options
	 *
	 * @var string
	 */
	protected $_options;

	/**
	 * Constructor. Init all variables.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array())
	{
		$this->_options = ($this->_default($options, array
					(
					'host' => '127.0.0.1',
					'port' => 1042,
					'timeout' => 10,
				)));

		$this->_h = NULL;
		$this->_open();
	}

	/**
	 * returns value of variable in shared mem
	 *
	 * @param string $name name of variable
	 *
	 * @return mixed value of the variable
	 * @access public
	 */
	public function get($name, $default = NULL)
	{
		$this->_open();
		$s = 'F'.pack('N', strlen($name)).$name;
		fwrite($this->_h, $s);

		for ($data = '';  ! feof($this->_h);)
		{
			$data .= fread($this->_h, 4096);
		}

		$this->_close();

		return $data === '' ? $default : unserialize($data);
	}

	/**
	 * set value of variable in shared mem
	 *
	 * @param string $name  name of the variable
	 * @param string $value value of the variable
	 * @param int $ttl (optional) time to life of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function set($name, $value)
	{
		$this->_open();
		$value = serialize($value);
		$s = 'S'.pack('NN', strlen($name), strlen($value)).$name.$value;

		fwrite($this->_h, $s);
		$ret = fgets($this->_h);
		$this->_close();

		return $ret === "OK\n";
	}

	/**
	 * remove variable from memory
	 *
	 * @param string $name  name of the variable
	 *
	 * @return bool TRUE on success
	 */
	public function rm($name)
	{
		$this->_open();
		$s = 'D'.pack('N', strlen($name)).$name;
		fwrite($this->_h, $s);
		$ret = fgets($this->_h);
		$this->_close();

		return $ret === "OK\n";
	}

	/**
	 * close connection to backend
	 * (sharedance isn't support persistent connection)
	 */
	protected function _close()
	{
		fclose($this->_h);
		$this->_h = FALSE;
	}

	/**
	 * open connection to backend if it doesn't connected yet
	 */
	protected function _open()
	{
		if ( ! is_resource($this->_h))
		{
			$this->_h = fsockopen($this->_options['host'], $this->_options['port'], $_, $_, $this->_options['timeout']);

			if ( ! is_resource($this->_h))
			{
				throw new Exception('Sharedance not connected');
			}
		}
	}

}

?>