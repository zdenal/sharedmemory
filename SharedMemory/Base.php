<?php

abstract class SharedMemory_Base
{

	abstract public function get($name, $default = NULL);

	abstract public function set($name, $value);

	abstract public function rm($name);

	/**
	 * returns name of current engine
	 *
	 * @return string name of engine
	 */
	public function engineName()
	{
		return strtolower(substr(get_class($this), 13));
	}

	/**
	 * fill non-set properties by def values
	 *
	 * @param array options array
	 * @param array hash of pairs keys and default values
	 *
	 * @return array filled array
	 */
	protected function _default(array $options = array(), array $def = array())
	{
		foreach ($def as $key => $val)
		{
			if ( ! isset($options[$key]))
			{
				$options[$key] = $val;
			}
		}

		return $options;
	}
	
	/**
	 * This method can be used in drivers not supported testing method to check
	 * if value exists.
	 * 
	 * @param	mixed	value to pack
	 * @return	array	value is contained as first and only item in array
	 */
	protected function _pack($value)
	{
		return array($value);
	}
	
	/**
	 * This method can be used in drivers not supported testing method to check
	 * if value exists.
	 * 
	 * @param	array	value constructed with _pack method
	 * @param	mixed	default value to return if unpack not successful
	 * @return	mixed	unpacked value
	 */
	protected function _unpack(array $value, $default = NULL)
	{
		if ( ! $this->_is_packed($value))
		{
			return $default;
		}
		return $value[0];
	}
	
	/**
	 * This method can be used in drivers not supported testing method to check
	 * if value exists.
	 * 
	 * @param	mixed	value to test
	 * @return	bool
	 */
	protected function _is_packed($value)
	{
		return is_array($value) AND count($value) === 1 AND isset($value[0]);
	}

}

?>