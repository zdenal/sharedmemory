<?php

require_once 'SharedMemory/Base.php';

class SharedMemory
{

	/**
	 * Create a new shared mem object
	 *
	 * @param string $type  the shared mem type (or FALSE on autodetect)
	 * @param array  $options  an associative array of option names and values
	 *
	 * @return SharedMemory_Base  a new SharedMemory_Base object
	 */
	public static function factory(array $options = array(), $type = FALSE)
	{
		if ($type === FALSE)
		{
			$type = SharedMemory::getAvailableTypes(TRUE);
		}
		else
		{
			$type = ucfirst(strtolower($type));
		}

		require_once 'SharedMemory/'.$type.'.php';
		$class = 'SharedMemory_'.$type;

		$ref = new $class($options);
		return $ref;
	}

	/**
	 * Get available types or first one
	 *
	 * @param bool $only_first FALSE if need all types and TRUE if only first one
	 *
	 * @return mixed list of available types (array) or first one (string)
	 */
	public static function getAvailableTypes($only_first = FALSE)
	{
		$detect = array
			(
			'eaccelerator' => 'Eaccelerator', // Eaccelerator (Turck MMcache fork)
			'mmcache' => 'Mmcache', // Turck MMCache
			'Memcache' => 'Memcache', // Memched
			//'shmop_open' => 'Shmop', // Shmop
			'apc_fetch' => 'Apc', // APC
			//'apache_note' => 'Apachenote', // Apache note
			'shm_get_var' => 'Systemv', // System V
			'sqlite_open' => 'Sqlite', // SQLite
			'file' => 'File', // Plain text
			'fsockopen' => 'Sharedance', // Sharedance
		);

		$types = array();

		foreach ($detect as $func => $val)
		{
			if (function_exists($func) OR class_exists($func))
			{
				if ($only_first)
				{
					return $val;
				}

				$types[] = $val;
			}
		}

		return $types;
	}

}

?>