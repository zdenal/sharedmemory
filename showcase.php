<?php

include_once 'SharedMemory.php';

$shm = SharedMemory::factory(array(
	'project' => 's'
));

echo $shm->engineName().'<br/>';

$shm->set('counter', $shm->get('counter') + 1);
echo $shm->get('counter');

//$shm->rm('counter');

?>